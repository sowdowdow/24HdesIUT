<footer class="page-footer orange">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Remerciements</h5>
        <p class="grey-text text-lighten-4">Merci aux organisateurs et aux bénévoles pour ce super évènement ! !</p>


      </div>
      <div class="col l6 s12">
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      Made with <i class="tiny material-icons pink-text">favorite</i> by <a href="#logo-container">Clair .Net et Précis</a> team
    </div>
  </div>
</footer>
