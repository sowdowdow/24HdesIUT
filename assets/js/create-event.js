function formChecker() {
var title = $('#title')[0].value;
var desc = $('#description')[0].value;
var date = $('#datepicker')[0].value;
var time = $('#time')[0].value;
var loc = $('#location')[0].value;
if(title == "" || desc == "" || date == "" || time == "" || loc == "") {
  //un champ est vide
  return true;
} else {
  return false;
}
}
$(document).ready(function(){
  $('.datepicker').datepicker({format:"yyyy-mm-dd"});

  //test des valeurs lors de l'envoi
  $('#submit').click(function (event) {
      if (formChecker()) {
        event.preventDefault();
        M.toast({html: 'Veuillez remplir tous les champs !'});
      }
  });

});
