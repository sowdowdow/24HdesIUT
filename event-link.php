<?php
  session_start();
  require_once 'assets/php/head.php';

  $lien = (!empty($_GET['link'])) ? $_GET['link'] : '';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
  <main>
    <div class="container">
      <br class="hide-on-med-and-down">
      <br class="hide-on-med-and-down">
      <div class="title center-align">
        <h3>Votre lien : <br><a class="carousel-item mono" href="<?= $lien ?>"><?= $lien ?></a></h3>
      </div>
      <br class="hide-on-med-and-down">
      <br>
      <div class="center-align">
        <img src="assets/php/imageGenerator.php?image=<?= $lien ?>&pixel=5&frame=2" alt="">
      </div>
    </div>
  </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

</html>
