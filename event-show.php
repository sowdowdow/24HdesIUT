<?php
  session_start();
  require_once 'assets/php/head.php';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
  <main>
      <div class="center-align">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d167998.10803373056!2d2.2069770643680573!3d48.85877410312378!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47e66e1f06e2b70f%3A0x40b82c3688c9460!2sParis!5e0!3m2!1sfr!2sfr!4v1527301591069" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <div id="map">

        </div>
      </div>
  </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

</html>
