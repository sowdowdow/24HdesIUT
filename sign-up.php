<?php
  session_start();
  require_once 'assets/php/head.php';
  require_once 'assets/php/databaseConnection.php';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
    <main>
      <div class="container">
          <h2 class="center-align">Créer un compte</h2>
          <div class="row">
            <form class="col s12" action="assets/php/create-user.POST.php" method="post">
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="John42" id="username" name="username" type="text" class="validate">
                  <label for="first_name">Pseudo</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input placeholder="example@gmail.com" id="username" name="email" type="email" class="validate">
                  <label for="first_name">Email</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="password" type="password" name="password" class="validate">
                  <label for="password">Mot de passe</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 right-align">
                  <button class="btn waves-effect waves-light orange" type="submit" name="submit">S'inscrire
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </form>
      </div>
    </main>
    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

  </html>
