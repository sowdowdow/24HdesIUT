<?php
  session_start();
  require_once 'assets/php/head.php';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
  <main>
    <div class="container">
      <div class="row">
        <h1 class="flow-text center-align"><i class="material-icons">date_range</i> Créé un évènement</h1>
        <form class="col s12" method="post" action="assets/php/create-event.POST.php">
          <div class="row">
            <div class="input-field col s12">
              <input id="title" type="text" class="validate" name="titre">
              <label for="title">Titre de l'évènement</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">description</i>
              <textarea id="description" class="materialize-textarea validate" name="desc"></textarea>
              <label for="description">Description</label>
            </div>
          </div>
          <div class="row">
            <div class="col s6 push-s3">
              <label for="datepicker">Date</label>
              <input id="datepicker" type="text" class="datepicker" name="date">
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">access_time</i>
              <input id="time" type="text" class="validate timepicker" name="heure">
              <label for="time">Heure</label>
            </div>
          </div>
          <div class="row">
            <div class="input-field col s12">
              <i class="material-icons prefix">place</i>
              <input id="location" type="text" class="validate" name="adresse">
              <label for="location">Lieu</label>
            </div>
          </div>
          <div class="center-align">
            <button id="submit" class="btn waves-effect waves-light" type="submit" name="action">Envoyer
              <i class="material-icons right">send</i>
            </button>
          </div>
        </form>
      </div>
    </div>
  </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';



    ?>

  </body>

</html>
