<?php
  session_start();
  require_once 'assets/php/head.php';
  require_once 'assets/php/databaseConnection.php';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
    <main>
      <div class="container">
          <h2 class="center-align">S'identifier</h2>
          <div class="row">
            <form class="col s12" action="assets/php/verif-signin.POST.php" method="post">
              <div class="row">
                <div class="input-field col s12">
                  <input id="first_name" name="username" type="text" class="validate">
                  <label for="first_name">Pseudo ou Email</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12">
                  <input id="password" type="password" name="password" class="validate">
                  <label for="password">Mot de passe</label>
                </div>
              </div>
              <div class="row">
                <div class="input-field col s12 right-align">
                  <a href="sign-up.php" class="left">Créer un compte</a>
                  <button class="btn waves-effect waves-light orange" type="submit" name="submit">Se connecter
                    <i class="material-icons right">send</i>
                  </button>
                </div>
              </div>
            </form>
          </div>
        </form>
      </div>
    </main>
    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

  </html>
