<?php
  /**
   * This class allow you to manipulate directly the users database.
   * HOW TO USE:
   *    1. initialize :
   *      - $user = new User();
   *    2. use it :
   *      - Create($username, $brutPassword, $email)
   *      - Load($userID)
   *      - LoadFromUsername($username)
   *      - Validate()
   *      - Delete()
   *      - SetPassword($brutPassword)
   *      - SetEmail($newEmail)
   *      - SetUsername($newUsername)
   *      - SetFirstname($newFirstname)
   *      - SetLastname($newLastname)
   *      - Dump()
   *
   * All these functions returns a state code.
   * To display it, simply place a 'echo' before the function.
   * i.e. :
   *  - echo $user->Load(528);
   *  - should return 'failedRetrievingUser' or 'OK' or 'emptyID'
   */
  class User
  {
      private $pdo;
      private $id;
      private $username;
      private $password;
      private $statut;

      // constructor
      public function __construct()
      {
          //creating a connection to the DB
          $this->pdo = new PDO('mysql:dbname=admin_24H;charset=UTF8;host=localhost', 'admin_admin_24H', 'prettyCoolPasswordIGuess');

          $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
      }
      //destructor
      public function __destruct()
      {
          return "Destroyed " . $this->id;
      }

      /**
      * creating an user and inserting it in the database
      * return the state :
      *   - 'error code' or 'OK'
      */
      public function Create($username, $brutPassword, $email)
      {
          //if a parameter is missing -> return error
          if (empty($username) || empty($brutPassword) || empty($email)) {
              return 'emptyParameter';
          }

          //processing parameters
          $this->username = filter_var($username, FILTER_SANITIZE_STRING);
          $this->password = $this->HashPassword($brutPassword);
          $this->email = filter_var($email, FILTER_SANITIZE_EMAIL);

          //if a parameter is false -> return error
          if (empty($this->username) || empty($this->password) || empty($this->email)) {
              return 'failedProcessingParameters';
          }

          //check if user is not already existing
          $request = $this->pdo->prepare('SELECT * FROM users WHERE username = :username');
          $request->execute(array(
            ':username' => $username
          ));

          $usersRetrieved = $request->fetchAll();

          if (count($usersRetrieved) > 0) {
              return 'usernameAlreadyExist';
          }


          //preparing the query
          $preparedQuery = $this->pdo->prepare('INSERT INTO users(username, email, password, creationdate, validated) VALUES (:username, :email, :password, :creationdate, :validated)');

          //initializing creation date and validated prop.
          $this->creationdate = date('Y-m-d H:i:s', time());
          $this->validated = 0;

          //executing it
          $preparedQuery->execute(array(
            ':username' => $this->username,
            ':email' => $this->email,
            ':password' => $this->password,
            ':creationdate' => $this->creationdate,
            ':validated' => $this->validated
          ));

          //initializing the ID of the created user
          $this->id = $this->pdo->lastInsertId();

          return 'OK';
      }

      //Load an user
      public function Load($userID)
      {
          //if an user ID is passed
          if (!empty($userID)) {

            //retrieving the corresponding user in DB
              $request = $this->pdo->prepare('SELECT * FROM users WHERE id = :id');
              $request->execute(array(':id' => $userID));

              $userRetrieved = $request->fetchAll();

              if (count($userRetrieved) != 1) {
                  return 'failedRetrievingUser';
              } else {
                  $errors = $this->Initialize($userRetrieved[0]);
                  return 'OK';
              }
          } else {
              return 'emptyID';
          }
      }

      //Load from username
      public function LoadFromUsername($username, $password)
      {
          //if an user ID is passed
          if (empty($username)) {
              return 'emptyUsername';
          }
          if (empty($password)) {
              return 'emptyPassword';
          }


          //retrieving the corresponding user in DB
          $request = $this->pdo->prepare('SELECT * FROM users WHERE username = :username AND password = :password');
          $request->execute(array(
            ':username' => $username,
            ':password' => $this->HashPassword($password)));

          $userRetrieved = $request->fetchAll();

          if (count($userRetrieved) != 1) {
              return 'failedRetrievingUser';
          } else {
              $errors = $this->Initialize($userRetrieved[0]);
              return 'OK';
          }
      }

      /**
      *  This function validate an user account.
      *  It should be used after a click on an email confiramtion link i.e.
      */
      public function Validate()
      {
          //retrieving the corresponding user in DB
          $request = $this->pdo->prepare('SELECT validated FROM users WHERE id = :id');
          $request->execute(array(':id' => $this->id));

          $userRetrieved = $request->fetchAll();

          //if the user is not already valid
          if (empty($userRetrieved[0]['validated'])) {
              $this->pdo->query("UPDATE users SET validated = 1 WHERE id = $this->id");
              $this->validated = '1';
              return 'OK';
          } else {
              return 'userAlreadyValid';
          }
      }

      /**
      *  Delete an user from the database and return a state code :
      *  - OK
      *  - FailedToDeleteEmptyUser
      *  - FailedToDeleteMultipleUserDetected
      */
      public function Delete()
      {
          //checking if user exist
          $existenceCheck = $this->pdo->prepare('SELECT COUNT(id) FROM users WHERE id = :id');
          $existenceCheck->execute(array(':id' => $this->id));

          switch ($existenceCheck->fetchAll()[0]['COUNT(id)']) {

            case '1':
              //deleting the user
              $deletingRequest = $this->pdo->prepare('DELETE FROM users WHERE id = :id');
              $deletingRequest->execute(array(':id' => $this->id));
              $this->__destruct();
              return 'OK';
              break;

            case '0':
              return 'FailedToDeleteEmptyUser';
              break;

            default:
              return 'FailedToDeleteMultipleUserDetected';
              break;
          }
      }

      /**
      *  This function allow you to simply change the password of an user
      *  give it the brut password, and that's it !
      */
      public function SetPassword($brutPassword)
      {
          $this->password = $this->HashPassword($brutPassword);
          $changingPassRequest = $this->pdo->prepare("UPDATE users SET password = :password WHERE id = :id");
          $changingPassRequest->execute(
              array(
            ':password' => $this->password,
            ':id' => $this->id)
          );

          return 'OK';
      }

      /**
      * set a new email for an user
      * state codes :
      *  - 'OK'
      *  - 'InvalidEmailParameter'
      */
      public function SetEmail($newEmail)
      {
          $sanitizedEmail = filter_var($newEmail, FILTER_SANITIZE_EMAIL);

          //check if the entered parameter is a valid email
          if ($sanitizedEmail) {

              //set the new password in the DB
              $changingEmailRequest = $this->pdo->prepare("UPDATE users SET email = :email WHERE id = :id");
              $changingEmailRequest->execute(
              array(
                ':email' => $sanitizedEmail,
                ':id' => $this->id)
              );

              //set the new password in the object
              $this->email = $sanitizedEmail;

              return 'OK';
          } else {
              return 'InvalidEmailParameter';
          }
      }

      /**
      * change the username for an user
      * state codes :
      *  - badSanitization
      *  - invalidCharacters
      *  - tooShort
      *  - tooLong
      *  - OK
      */
      public function SetUsername($newUsername)
      {
          $sanitizedUsername = filter_var($newUsername, FILTER_SANITIZE_STRING);
          if (!$sanitizedUsername) {
              return 'badSanitization';
          }
          if (!preg_match('/[\w\dàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ]+/', $sanitizedUsername)) {
              return 'invalidCharacters';
          }
          if (strlen($sanitizedUsername) < 3) {
              return 'tooShort';
          }
          if (strlen($sanitizedUsername) > 30) {
              return 'tooLong';
          }

          //set the new password in the DB
          $changingUsernameRequest = $this->pdo->prepare("UPDATE users SET username = :username WHERE id = :id");
          $changingUsernameRequest->execute(
            array(
              ':username' => $sanitizedUsername,
              ':id' => $this->id)
            );

          //set the new password in the object
          $this->username = $sanitizedUsername;

          return 'OK';
      }

      /**
      * change the firstname for an user
      * state codes :
      *  - badSanitization
      *  - invalidCharacters
      *  - tooShort
      *  - tooLong
      *  - OK
      */
      public function SetFirstname($newFirstname)
      {
          $sanitizedFirstname = filter_var($newFirstname, FILTER_SANITIZE_STRING);
          if (!$sanitizedFirstname) {
              return 'badSanitization';
          }
          if (!preg_match('/[\wàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ]+/', $sanitizedFirstname)) {
              return 'invalidCharacters';
          }
          if (strlen($sanitizedFirstname) < 2) {
              return 'tooShort';
          }
          if (strlen($sanitizedFirstname) > 50) {
              return 'tooLong';
          }

          //set the new password in the DB
          $changingUsernameRequest = $this->pdo->prepare("UPDATE users SET firstname = :firstname WHERE id = :id");
          $changingUsernameRequest->execute(
            array(
              ':firstname' => $sanitizedFirstname,
              ':id' => $this->id)
            );

          //set the new password in the object
          $this->firstname = $sanitizedFirstname;

          return 'OK';
      }

      /**
      * change the lastname for an user
      * state codes :
      *  - badSanitization
      *  - invalidCharacters
      *  - tooShort
      *  - tooLong
      *  - OK
      */
      public function SetLastname($newLastname)
      {
          $sanitizedLastname = filter_var($newLastname, FILTER_SANITIZE_STRING);
          if (!$sanitizedLastname) {
              return 'badSanitization';
          }
          if (!preg_match('/[\wàèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸçÇßØøÅåÆæœ]+/', $sanitizedLastname)) {
              return 'invalidCharacters';
          }
          if (strlen($sanitizedLastname) < 2) {
              return 'tooShort';
          }
          if (strlen($sanitizedLastname) > 50) {
              return 'tooLong';
          }

          //set the new password in the DB
          $changingUsernameRequest = $this->pdo->prepare("UPDATE users SET lastname = :lastname WHERE id = :id");
          $changingUsernameRequest->execute(
            array(
              ':lastname' => $sanitizedLastname,
              ':id' => $this->id)
            );

          //set the new password in the object
          $this->lastname = $sanitizedLastname;

          return 'OK';
      }

      //this function allows you to simply dump
      // an user in order to DEBUG
      public function Dump()
      {
          echo "<pre style=\"color: #f00; font-size: 1.5em;\">";
          var_dump($this);
          echo "</pre>";
      }

      //this function hash a brut password
      //called by 'Create($username, $brutPassword, $email)'
      private function HashPassword($brutPassword)
      {
          $salted = $brutPassword . 'Vive les coquillettes';
          return hash('sha256', $salted);
      }

      //this function will initialize an user
      private function Initialize($arrayOfProperties)
      {
          //initialize an object from an array of properties
          foreach ($arrayOfProperties as $key => $value) {
              $this->{$key} = $value;
          }
          return 'OK';
      }
  }
