<?php
  session_start();
  require_once 'assets/php/head.php';

  $id = filter_var($_GET['id'], FILTER_SANITIZE_NUMBER_INT);
  if (!$id) {
      header('location: index.php?error');
  }

  $bd = new PDO('mysql:dbname=admin_24H;charset=UTF8;host=localhost', 'admin_admin_24H', 'prettyCoolPasswordIGuess');
  $stmt = $bd->prepare("SELECT * FROM evenement WHERE ID=:id");
  $stmt->execute(array(':id' => $id));
  $result = $stmt->fetch();

  $stmt = $bd->prepare("SELECT * FROM participant WHERE idevent=:id");
  $stmt->execute(array(':id' => $result["ID"]));
  $participants = $stmt->fetchAll();
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
  <main>
    <div class="container">
      <div class="align-center">
        <h3><?php echo $result['Titre']; ?></h3>
        <!-- event desc -->
        <ul class="collection">
          <li class="collection-item"><?php echo $result['Description']; ?></li>
          <li class="collection-item"><?php echo $result['date']; ?></li>
          <li class="collection-item"><?php echo $result['heure']; ?></li>
          <li class="collection-item"><?php echo $result['adresse']; ?></li>
          <li class="collection-item"><?php echo $result['id_createur']; ?></li>
        </ul>
        <!-- form -->
        <form class="col s12" method="post" action="assets/php/answerAtAnwsersLink.POST.php">
          <input type="hidden" name="idEvenement" value="<?php echo $result["ID"]; ?>">
          <div class="row">
            <div class="input-field col s12">
              <input id="name" type="text" class="validate" name="name">
              <label for="name">Nom</label>
            </div>
          </div>
          <div class="row center-align">
            <div class="col s12 m6">
              <button class="deletingButton waves-effect waves-light btn-large red" type="submit" name="refuse"> <i class="material-icons left">close</i>Je ne participe pas</button>
            </div>
            <div class="col s12 m6">
              <br class="hide-on-med-and-up">
              <button class="validateButton waves-effect waves-light btn-large green" type="submit" name="accept" > <i class="material-icons left">check</i>Je participe</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <!-- floating button -->
    <div class="fixed-action-btn">
      <a class="btn-floating btn-large teal">
        <i class="large material-icons">keyboard_arrow_up</i>
      </a>
      <ul>
        <li><a class="btn-floating yellow darken-1 modal-trigger" href="#modal1"><i class="material-icons">people</i></a></li>
      </ul>
    </div>

    <!-- Modal Trigger -->

    <!-- Modal Structure -->
    <div id="modal1" class="modal bottom-sheet">
      <div class="modal-content">
        <h4>Participants</h4>
        <ul class="collection">
        <?php foreach ($participants as $key=>$value){ ?>
          <li class="collection-item avatar">
            <img src="images/yuna.jpg" alt="" class="circle">
            <p><?php if(!$value["nom"] == ""){echo $value["nom"];}else{echo "Nom non renseigné";} ?></p>
            <p><?php if(!$value["commentaire"] == ""){echo $value["commentaire"];}else{echo "Pas de commentaire";} ?></p>
            <p><?php if($value["participe"] == true) { echo "Participe à l'événement"; } else { echo "Ne participe pas";}?> </p>
          </li>
        <?php } ?>
        </ul>
      </div>
      <div class="modal-footer">
        <a href="#!" class="modal-close waves-effect waves-green btn-flat">Agree</a>
      </div>
    </div>
  </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

  <script>
  
  </script>

</html>
