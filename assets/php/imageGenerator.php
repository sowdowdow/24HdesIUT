<?php
/*==========================
pour utiliser :
passer en $_GET :
- image = lien
- pixel = taille des pixels
- frame = taille du contour
===========================*/
  if (!empty($_GET['image'])) {
      require_once $_SERVER['DOCUMENT_ROOT'] . '/24hdesIUT/assets/phpqrcode/qrlib.php';
      $text = $_GET['image'];
      $size = 3;
      $frame = 4;

      if (!empty($_GET['pixel'])) {
          $size = $_GET['pixel'];
      }
      if (!empty($_GET['frame'])) {
          $frame = $_GET['frame'];
      }

      QRcode::png($text, false, $level = QR_ECLEVEL_L, $size, $frame, false);
  }
