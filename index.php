<?php
  session_start();
  require_once 'assets/php/head.php';
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
    <div class="section no-pad-bot" id="index-banner">
      <div class="container">
        <br><br>
        <h1 class="header center orange-text">Clair .Net et Précis</h1>
        <div class="row center">
          <h5 class="header col s12 light">Développement Web (22h-6h)</h5>
        </div>
        <div class="row center">
          <a href="sign-in.php" id="download-button" class="btn-large waves-effect waves-light orange">Démarrer l'aventure</a>
        </div>
        <br><br>

      </div>
    </div>

    <main>
      <div class="container">
        <div class="section">

          <!--   Icon Section   -->
          <div class="row">
            <div class="col s12 m4">
              <div class="icon-block">
                <h2 class="center red-text"><i class="material-icons">flash_on</i></h2>
                <h5 class="center">Clair</h5>

                <p class="light">
                  La seconde épreuve est une <span class="red-text">épreuve de développement Web</span> : il est demandé aux équipes de de développer une application web à partir de fonctionnalités fournies dans un cahier des charges, d'installer et de
                  maintenir le serveur.
                </p>
              </div>
            </div>

            <div class="col s12 m4">
              <div class="icon-block">
                <h2 class="center light-blue-text"><i class="material-icons">group</i></h2>
                <h5 class="center">Net</h5>

                <p class="light">Le classement est établi par un jury qui examine l'ensemble des applications et les évalue à partir d'une liste de critères : conformité au cahier des charges, fonctionnalités réalisées, qualité de l'interface, sécurité ...</p>
              </div>
            </div>

            <div class="col s12 m4">
              <div class="icon-block">
                <h2 class="center light-green-text"><i class="material-icons">settings</i></h2>
                <h5 class="center">Précis</h5>

                <p class="light">Par exemple, lors de l'édition 2014, il fallait développer un site permettant de gérer les dépenses entre amis lors de sorties/événements et savoir qui devait combien à qui. En 2015, il s'agissait de développer une interface qui permettait
                  aux manutentionnaires du port du Havre de gérer les départs/arrivées des bateaux. L'épreuve de 2017 consistait à développer une application pour gérer un mini-réseau social.</p>
              </div>
            </div>
          </div>

        </div>
        <br><br>
      </div>
    </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

  </html>
