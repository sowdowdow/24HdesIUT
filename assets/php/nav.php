<?php
  $pages = array();
    if (isset($_SESSION['id'])) {
        $pages['event-list.php'] = '<i class="material-icons left">format_list_bulleted</i> Mes évènements';
        $pages['create-event.php'] = '<i class="material-icons left">create</i> Créer un évènement';
        $pages['disconnect.php'] = '<i class="material-icons right">exit_to_app</i> Déconnexion';
    } else {
        $pages['sign-in.php'] = '<i class="material-icons left">fingerprint</i> Connexion';
    }
 ?>
<header>
  <nav class="orange lighten-1" role="navigation">
    <!-- classic navbar -->
    <div class="nav-wrapper container"><a id="logo-container" href="index.php" class="brand-logo">Clair-Net-Précis App</a>
      <ul class="right hide-on-med-and-down">
        <?php if (isset($_SESSION['id'])) {
     ?>
        <li>
          <a>Bonjour <?php echo $_SESSION['username']; ?></a>
        </li>
        <?php
 } ?>
        <?php foreach ($pages as $link => $title): ?>
        <li>
          <a href="<?php echo $link; ?>">
            <?php echo $title; ?>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>

      <!-- mobile sidenav -->
      <ul id="nav-mobile" class="sidenav">
        <?php foreach ($pages as $link => $title): ?>
        <li>
          <a href="<?php echo $link; ?>">
            <?php echo $title; ?>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
      <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
    </div>
  </nav>
</header>
