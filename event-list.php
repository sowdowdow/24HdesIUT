<?php
  session_start();
  require_once 'assets/php/head.php';


  $bd = new PDO('mysql:dbname=admin_24H;charset=UTF8;host=localhost', 'admin_admin_24H', 'prettyCoolPasswordIGuess');
  $stmt = $bd->prepare("INSERT INTO evenement VALUES(null,$titre,$desc,$date,$heure,null,1,null,$adresse)");
  $stmt->execute();

  $stmt = $bd->query("SELECT * FROM evenement ORDER BY ID DESC");
  $records = $stmt->fetchAll();
?>


  <body>
    <?php require_once 'assets/php/nav.php'; ?>
  <main>
    <div class="container">
      <ul class="collection with-header">
        <?php foreach ($records as $record): ?>
          <?php $eventLink = 'https://clair-net-precis.tk/event-answer.php?id='.$record['ID']; ?>
          <li class="collection-header">
            <h3><?= $record['Titre'] ?></h3>
            <a href="https://clair-net-precis.tk/event-link.php?link=<?= $eventLink ?>"><i class="material-icons right">send</i></a>
            <ul class="collection">
              <li class="collection-item"><?= $record['Description'] ?></li>
              <li class="collection-item"><?= $record['heure'] ?></li>
              <li class="collection-item"><?= $record['adresse'] ?></li>
            </ul>
          </li>
        <?php endforeach; ?>
      </ul>
    </div>
  </main>

    <?php
      require_once 'assets/php/footer.php';
      require_once 'assets/php/scripts.php';
    ?>

  </body>

</html>
