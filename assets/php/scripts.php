<!--  Scripts-->
<script src="assets/js/jquery-3.3.1.min.js"></script>
<script src="assets/js/materialize.min.js"></script>
<script src="assets/js/init.js"></script>
<?php
  //load automatically the associated JS to a PHP page (same name)
  $scriptName = basename($_SERVER['PHP_SELF'], '.php');
  echo '<script src="assets/js/' . $scriptName . '.js"></script>';
 ?>
